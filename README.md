cookbook-yii
============

Installs Yii PHP framework

Requirements
============

* git

Attributes
==========

* node['yii']['repository'] - Git repository to checkout, default: git://github.com/yiisoft/yii.git
* node['yii']['revision'] - Revision to checkout, default: master
* node['yii']['install_path'] - Install base path, default: /usr/local/lib/php

Recipes
=======

## default

Clone and checkout yii framework.
Framework will be installed to node['yii']['install_path']/yii-node['yii']['revision']
