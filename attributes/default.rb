default['yii']['repository'] = 'git://github.com/yiisoft/yii.git'
default['yii']['revision'] = 'master'
default['yii']['install_path'] = '/usr/local/lib/php'