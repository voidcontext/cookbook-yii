include_recipe 'git'

path = node.yii.install_path + '/yii-' + node.yii.revision

directory path do
	owner "root"
	mode "0755"
	recursive true
	action :create
end

git path do 
	repository node.yii.repository
	revision node.yii.revision
	action :sync
end
