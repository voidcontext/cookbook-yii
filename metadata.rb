name              "yii"
maintainer        "Istvan Szenasi"
maintainer_email  "szeist@gmail.com"
license           "MIT"
description       "Installs Yii PHP framework"
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           "0.1"
recipe            "yii", "Installs yii framework"
depends           "git"

attribute "yii",
  :display_name => "Yii hash",
  :description => "Yii confirugation hash",
  :type => "hash"

attribute "yii/repository",
  :display_name => "Git repository",
  :description => "Yii source git repostiory",
  :default => "git://github.com/yiisoft/yii.git"

attribute "yii/revision",
  :display_name => "Revision",
  :description => "Revision to checkout",
  :default => "master"

attribute "yii/install_path",
  :display_name => "Install path",
  :description => "Path to install framework",
  :default => "/usr/local/lib/php"

